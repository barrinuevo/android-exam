package com.example.raymond.androiddeveloperchallenge.network

import io.reactivex.Flowable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface BaseProjectAPI {

    @GET("popular?api_key=a16c56dbd6a66df9795d61fa6e5c9fb8")
    fun getRaw(): Call<String>
}