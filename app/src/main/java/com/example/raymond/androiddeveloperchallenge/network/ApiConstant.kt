package com.example.raymond.androiddeveloperchallenge.network

class ApiConstant() {
    val INTERNAL_SERVER_ERROR = 500
    val NO_NETWORK = -1
    internal val RUN_TIME_EXCEPTION = -2
}