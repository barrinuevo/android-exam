package com.example.raymond.androiddeveloperchallenge.core.contract

import com.example.raymond.androiddeveloperchallenge.modules.movie_list.model.MovieList

interface BaseContract {

    interface View {
        fun showMessage(message: String)

        fun showRawData(raw: MovieList)
    }

    interface Presenter {

        fun attachView(view: View)

        fun detachView()

        fun clear()

        fun getRawData()
    }
}