package com.example.raymond.androiddeveloperchallenge.core.utils

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File
import java.net.URL


class ImageLoader {
    val constantImageLink = "https://image.tmdb.org/t/p/w500"
    fun loadImageFromUrl(pathImage: String, view: ImageView, context: Context?) {
        context?.let {
            val url = URL(Utils().imageURLFormatter(constantImageLink + pathImage))
            val uri = Uri.parse(url.toURI().toString())
            Glide.with(it)
                    .load(uri)
                    .into(view)
        }
    }
}