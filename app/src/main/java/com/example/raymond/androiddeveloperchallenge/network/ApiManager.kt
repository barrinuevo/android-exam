package com.example.raymond.androiddeveloperchallenge.network

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager {
    private var mInstance: ApiManager? = null
    var baseProjectAPI: BaseProjectAPI? = null

    constructor() {
        this.init()
    }

    fun getInstance(): ApiManager? {
        if (mInstance == null) {
            mInstance = ApiManager()
        }
        return mInstance
    }

    private fun init() {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        this.baseProjectAPI = retrofit.create(BaseProjectAPI::class.java)
    }
}