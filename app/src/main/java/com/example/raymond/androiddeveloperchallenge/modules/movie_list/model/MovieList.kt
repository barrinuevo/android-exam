package com.example.raymond.androiddeveloperchallenge.modules.movie_list.model

import com.google.gson.annotations.SerializedName

class MovieList(@SerializedName("page") val _page: Int,
                @SerializedName("total_results") val _total_results: Int,
                @SerializedName("total_pages") val _total_pages: String,
                @SerializedName("results") val _movies: ArrayList<Movie>) {

    class Movie(_vote_count: Int, _id: Int, _video: Boolean, _vote_average: Float, _title: String, _popularity: Float,
                _poster_path: String, _original_language: String, _original_title: String, _genre_ids: IntArray,
                _backdrop_path: String, _adult: Boolean, _overview: String, _release_date: String) {

        companion object {
            fun create(_vote_count: Int, _id: Int, _video: Boolean, _vote_average: Float, _title: String, _popularity: Float,
                       _poster_path: String, _original_language: String, _original_title: String, _genre_ids: IntArray,
                       _backdrop_path: String, _adult: Boolean, _overview: String, _release_date: String):
                    Movie = Movie(_vote_count, _id, _video, _vote_average, _title, _popularity,
                    _poster_path, _original_language, _original_title, _genre_ids,
                    _backdrop_path, _adult, _overview, _release_date)
        }

        var vote_count: Int = _vote_count
        var id: Int = _id
        var video: Boolean = _video
        var vote_average: Float = _vote_average
        var title: String = _title
        var popularity: Float = _popularity
        var poster_path: String = _poster_path
        var original_language: String = _original_language
        var original_title: String = _original_title
        var genre_ids: IntArray = _genre_ids
        var backdrop_path: String = _backdrop_path
        var adult: Boolean = _adult
        var overview: String = _overview
        var release_date: String = _release_date

    }
}