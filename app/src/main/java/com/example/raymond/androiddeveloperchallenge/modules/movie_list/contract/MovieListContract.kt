package com.example.raymond.androiddeveloperchallenge.modules.movie_list.contract

import com.example.raymond.androiddeveloperchallenge.core.contract.BaseContract

class MovieListContract {

    interface View : BaseContract.View {
    }

    interface Presenter : BaseContract.Presenter {

    }
}