package com.example.raymond.androiddeveloperchallenge.modules.movie_list.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.raymond.androiddeveloperchallenge.R
import com.example.raymond.androiddeveloperchallenge.core.utils.ImageLoader
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.model.MovieList
import kotlinx.android.synthetic.main.item_pin_board.view.*
import kotlin.collections.ArrayList

class MovieListAdapter(val context: Context?, var items: ArrayList<MovieList.Movie>) : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {
    var localItems = items

    fun filterTitle(text: String) {
        var newItems: ArrayList<MovieList.Movie> = arrayListOf()
        if (!text.isEmpty()) {
            for (newItem in localItems!!) {
                if (newItem.original_title.toLowerCase().contains(text.toLowerCase())) {
                    newItems.add(newItem)
                }
            }
            items = newItems
        }
        notifyDataSetChanged()
    }

    fun getAllItem(): ArrayList<MovieList.Movie> {
        return localItems
    }

    fun addAllItem(localItem: ArrayList<MovieList.Movie>) {
        items.clear()
        items.addAll(localItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_pin_board, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MovieListAdapter.ViewHolder, position: Int) {
        val item = items[position]

        ImageLoader().loadImageFromUrl(item.poster_path, holder.image_background, context)
        holder.txt_title?.text = item.original_title
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image_background = itemView.image_background
        val txt_title = itemView.txt_title

    }
}