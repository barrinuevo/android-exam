package com.example.raymond.androiddeveloperchallenge.modules.movie_list.view

import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.example.raymond.androiddeveloperchallenge.R
import com.example.raymond.androiddeveloperchallenge.core.customview.ProgressDialog
import com.example.raymond.androiddeveloperchallenge.core.utils.Utils
import com.example.raymond.androiddeveloperchallenge.core.view.BaseAppFragment
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.adapter.MovieListAdapter
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.contract.MovieListContract
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.model.MovieList
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.presenter.MovieListPresenter
import kotlinx.android.synthetic.main.fragment_pin_board.*


open class MovieListFragment : BaseAppFragment<MovieListContract.Presenter>(), MovieListContract.View {

    var progressDialog: ProgressDialog? = null
    private var movieListAdapter: MovieListAdapter? = null

    override fun showRawData(raw: MovieList) {
        rcv_board.layoutManager = LinearLayoutManager(context)
        rcv_board.isNestedScrollingEnabled = false
        movieListAdapter = MovieListAdapter(context, raw._movies)
        rcv_board.adapter = movieListAdapter
        rcv_board.setLayoutManager(GridLayoutManager(activity?.getApplicationContext(), 2))
        progressDialog?.hideProgress()
    }

    override fun createPresenterInstance(): MovieListContract.Presenter {
        return MovieListPresenter(context)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_pin_board
    }

    override fun initViews() {
        if (Utils().isInternetOn(context)) {
            progressDialog = ProgressDialog(context)
            progressDialog?.showProgress()
            presenter?.getRawData()
        } else {
            Toast.makeText(context, getString(R.string.err_no_network), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        img_searh.setOnClickListener {
            if (ll_search.visibility == View.INVISIBLE) {
                ll_search.visibility = View.VISIBLE
            } else {
                ll_search.visibility = View.INVISIBLE
            }
        }
        edt_searh.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    val value = p0.toString()
                    if (!"".contentEquals(value)) {
                        movieListAdapter?.filterTitle(p0.toString())
                    } else {
                        movieListAdapter?.addAllItem(getValueOfEditTextInAdapter())
                    }
                } else {
                    movieListAdapter?.addAllItem(getValueOfEditTextInAdapter())
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    fun getValueOfEditTextInAdapter(): ArrayList<MovieList.Movie> {
        var position = 0
        val newItems = ArrayList<MovieList.Movie>()
        for (movie in movieListAdapter?.getAllItem()!!) {
            newItems.add(movie)
            position++
        }
        return newItems
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        progressDialog?.hideProgress()
    }
}