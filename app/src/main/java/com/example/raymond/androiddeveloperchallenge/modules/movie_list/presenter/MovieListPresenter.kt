package com.example.raymond.androiddeveloperchallenge.modules.movie_list.presenter

import android.content.Context
import com.example.raymond.androiddeveloperchallenge.core.presenter.BasePresenter
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.contract.MovieListContract
import com.example.raymond.androiddeveloperchallenge.modules.movie_list.model.MovieList
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieListPresenter(activity: Context?) : BasePresenter(activity), MovieListContract.Presenter {

    private val gson: Gson = Gson()

    override fun getRawData() {
        getApi()?.getRaw()?.enqueue(object : Callback<String> {

            override fun onResponse(call: Call<String>, response: Response<String>) {
                val pinBoard = gson.fromJson<MovieList>(response.body(), MovieList::class.java)
                getView()?.showRawData(pinBoard)
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                System.out.print(t.toString())
            }
        })
    }
}