package com.example.raymond.androiddeveloperchallenge.core.customview

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.View
import com.example.raymond.androiddeveloperchallenge.R
import com.example.raymond.androiddeveloperchallenge.core.view.BaseDialog
import kotlinx.android.synthetic.main.dialog_notify.*

class NotifyDialog : BaseDialog() {

    private var title: String? = null
    private var message: String? = null
    private var isShowCancel: Boolean = false
    private var listener: OnNotifyCallback? = null
    private var positiveTitle: String? = null
    private var negativeTitle: String? = null
    private var isChangeButtonPosition: Boolean = false

    interface OnNotifyCallback {
        fun onLeftButtonClick()

        fun onRightButtonClick(vararg obj: Any)
    }

    fun showDialog(fragmentManager: FragmentManager, title: String?,
                   message: String?, positiveTitle: String?, negativeTitle: String?,
                   isShowCancel: Boolean, isChangeButtonPosition: Boolean, listener: OnNotifyCallback?) {
        val notifyDialog = NotifyDialog()

        notifyDialog.title = title
        notifyDialog.message = message
        notifyDialog.isShowCancel = isShowCancel
        notifyDialog.positiveTitle = positiveTitle
        notifyDialog.negativeTitle = negativeTitle
        notifyDialog.listener = listener
        notifyDialog.isChangeButtonPosition = isChangeButtonPosition
        notifyDialog.retainInstance = true

        notifyDialog.show(fragmentManager, NotifyDialog::class.java.simpleName)
    }

    override fun getDialogLayout(): Int {
        return R.layout.dialog_notify
    }
}